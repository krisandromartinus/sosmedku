<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'body' => 'required|max:255',
            'file' => 'image|max:2048',
        ]);

        if ($validator->fails()) {

            return back()->with('errors', $validator->messages()->all()[0])->withInput();

        }

        $post = new Post;

        if ($request->has('file')) {
            $path = Storage::putFileAs(
                'public/postImage',
                $request->file('file'),
                Str::random(mt_rand(10, 15)) . '.' . $request->file('file')->getClientOriginalExtension()
            );
            $post->picture = $path;
        } else {
            $post->picture = '';
        }

        $post->user_id = Auth::id();
        $post->body = $request->body;
        $process = $post->save();

        if ($process) {
            return redirect()->back()->withSuccess("Sukses Membuat Post !");
        } else {
            return redirect()->back()->withErrors("Gagal Membuat Post");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

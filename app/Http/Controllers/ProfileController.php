<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = Auth::id();

        $total_follow = User::find($authUser);
        $total_follow_process = $total_follow->follow->count();
        $total_followed_process = $total_follow->followed->count();

        $profile_details = User::find($authUser);
        $profile_details_process = $profile_details->profile;

        $post_list = Post::where('user_id', $authUser)
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('page.profile.index', compact('total_follow_process', 'profile_details_process', 'total_followed_process', 'post_list'));
    }

    public function profileData($profile)
    {

        $authUser = Auth::id();

        $user = User::select('*')->where('name', $profile)->first();

        if ($user === null) {
            return redirect('profile');
        }

        $total_follow_process = $user->follow->count();
        $total_followed_process = $user->followed->count();

        $check_followed = Follow::where('user_id', $authUser)
            ->where('followed_id', $user->id)
            ->get()
            ->isEmpty();

        $profile_details_process = $user->profile;

        return view('page.profile.indexID', compact('total_follow_process', 'profile_details_process', 'total_followed_process', 'check_followed'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

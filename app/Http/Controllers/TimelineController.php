<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Like;
use Illuminate\Support\Facades\Auth;

class TimelineController extends Controller
{
    public function index(){
        $userAuth = Auth::id();
        $user = User::select('*')
        ->where('id', $userAuth)
        ->orderBy('created_at', 'ASC')
        ->get();


        return view('page.timeline.index', compact('user'));
    }
}

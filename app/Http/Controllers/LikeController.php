<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function likePost($id){

        $like = new Like;
        $like->user_id = Auth::id();
        $like->post_id = $id;
        $like->save();
        
        return back()->with('success', 'Success Like Post User');
    }
}

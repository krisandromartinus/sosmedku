<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchUser(Request $request)
    {

        if (!is_null($request->search)) {
            $name = $request->search;
            $user = User::where('name', 'LIKE', '%' . $name . '%')->get();
            $user_search = $user;

            return view('page.search.index', compact('user_search'));

        } else {

            return redirect('profile');
        }

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('page.register.index');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'full_name' => 'required|max:50',
            'email' => 'required|unique:users|max:50',
            'password' => 'required|min:6|string',
            'retype_password' => 'same:password',
            'terms' => 'accepted',
        ]);

        if ($validator->fails()) {

            return back()->with('errors', $validator->messages()->all()[0])->withInput();

        }

        $process = User::create(
            [
                'name' => $request->full_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),

            ]
        );
        $process->profile()->create(
            [
                'full_name' => $request->full_name,
                'bio' => '',
                'photo_profile' => ''
            ]
        );

        return redirect('login')->with('success','Success Register , silahkan login');

    }
    
}

<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{

    public function FollowUser($id)
    {
        $follow = new Follow;
        $follow->user_id = Auth::id();
        $follow->followed_id = $id;
        $follow->save();

        return back()->with('success', 'Success Follow User ');
    }

    public function unfollowUser( $id)
    {
        $authUser = Auth::id();
        $check_followed = Follow::where('user_id', $authUser)
            ->where('followed_id', $id)
            ->get()
            ->isEmpty();

        if (!$check_followed){

            $follow = Follow::where('user_id', $authUser)
            ->where('followed_id', $id);
            $follow->delete();

        }

        return back()->with('success', 'Success Unfollow User ');

    }

    public function followList($id){
        $user = User::select('*')->where('id', $id)->get();
        $current_user = User::find($id);
        return view('page.follow.followers', compact('user','current_user'));
    }

    public function followedList($id){

        $user = User::select('*')->where('id', $id)->get();
        $current_user = User::find($id);
        return view('page.follow.followed', compact('user','current_user'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function show(Follow $follow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function edit(Follow $follow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Follow $follow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function destroy(Follow $follow)
    {
        //
    }
}

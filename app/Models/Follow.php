<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    use HasFactory;


    public function profile()
    {
        return $this->belongsTo(ProfileDetail::class, 'user_id');
    }
    public function profileSingle()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function profileSingles()
    {
        return $this->belongsTo(User::class, 'followed_id');
    }

    public function profilePosts()
    {
        return $this->hasMany(Post::class, 'user_id', 'followed_id');
    }

}

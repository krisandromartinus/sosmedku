<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne(ProfileDetail::class);
    }

    public function follow()
    {
        return $this->hasMany(Follow::class);
    }

    public function followed()
    {
        return $this->hasMany(Follow::class, 'followed_id');
    }

    public function checkFollow(){
        return $this->hasOne(Follow::class,'followed_id');
    }


    public function followedCheck()
    {
        return $this->hasMany(ProfileDetail::class, 'user_id');
    }






/*     public function detailSearch(){
        return $this->hasOne(ProfileDetail::class , 'user_id');
    } */
}

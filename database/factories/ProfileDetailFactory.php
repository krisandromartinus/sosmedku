<?php

namespace Database\Factories;

use App\Models\ProfileDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProfileDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProfileDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'full_name' => $this->faker->name(),
            'bio' => $this->faker->realText('10' ,'1'),
            'photo_profile' => '',
        ];
    }
}

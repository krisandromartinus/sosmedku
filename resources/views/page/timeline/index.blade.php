@extends('template.master')
@section('title', 'Index')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <center>
                <h1>Timeline</h1>
            </center>
            <div class="row mb-2">
                <div class="col-sm-6">
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Project Detail</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">


                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body" style="display: block;">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12">

                                @foreach ($user as $item_followed)

                                    @foreach ($item_followed->follow as $item)

                                        @foreach ($item->profilePosts as $item_post)
                                            <div class="post">
                                                <div class="user-block">
                                                    @if ($item_followed->profile->photo_profile == '')
                                                        <img src="https://ui-avatars.com/api/?name={{ $item->profileSingles->name }}"
                                                            class="img-circle elevation-2">
                                                    @else
                                                        <img src="{{ $item_followed->profile->photo_profile }}"
                                                            class="img-circle elevation-2">
                                                    @endif
                                                    <span class="username">
                                                        <a
                                                            href="{{ url('/', $item->profileSingles->name) }}">{{ $item->profileSingles->name }}</a>
                                                    </span>
                                                    <span class="description">{{ $item_followed->profile->bio }}</span>

                                                </div>
                                                <!-- /.user-block -->
                                                <p>
                                                    {{ $item_post->body }}
                                                </p>
                                                @if (!empty($item_post->picture))
                                                    <img class="img-fluid pad"
                                                        src="{{ Storage::url($item_post->picture) }}" alt="Photo">
                                                @else

                                                @endif
                                                <p>
                                                    <i class="fas fa-user-clock"></i> Diposting :
                                                    {{ Carbon\Carbon::parse($item_post->created_at)->diffForHumans() }}
                                                </p>

                                                <p>
                                                    <a href="{{ route('like.post', $item_post->id) }}" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                                </p>
                                            </div>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->

@endsection

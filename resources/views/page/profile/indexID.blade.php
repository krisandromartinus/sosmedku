@extends('template.master')
@section('title', 'Index')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile {{ $profile_details_process->full_name }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <div class="image">
                                @if ($profile_details_process->photo_profile == '')
                                    <img src="https://ui-avatars.com/api/?name={{ $profile_details_process->full_name }}"
                                        class="img-circle elevation-2">
                                @else
                                    <img src="{{ $profile_details_process->photo_profile }}"
                                        class="img-circle elevation-2">
                                @endif
                                <img src="" class="img-circle elevation-2">
                            </div>
                        </div>

                        <h3 class="profile-username text-center">{{ $profile_details_process->full_name }}</h3>
                        <input type="hidden" name="user_id" value="{{ $profile_details_process->id }}">
                        <p class="text-muted text-center">{{ $profile_details_process->bio }}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b><a href="{{ route('follow.list', $profile_details_process->id) }}"> Followers </a></b> <a class="float-right">{{ $total_followed_process }}</a>
                            </li>
                            <li class="list-group-item">
                                <b><a href="{{ route('following.list', $profile_details_process->id) }}"> Following </a></b> <a class="float-right">{{ $total_follow_process }}</a>
                            </li>
                        </ul>

                        @if ($check_followed)
                            <form action="{{ route('follow.user', $profile_details_process->id) }}" method="post">
                                @csrf

                                <button type="submit" class="btn btn-primary btn-block">Follow</button>
                            </form>

                        @else
                            <form action="{{ route('unfollow.user', $profile_details_process->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary btn-block">Unfollow</button>
                            </form>

                        @endif
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->

                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">

                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection

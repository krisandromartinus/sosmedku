@extends('template.master')
@section('title', 'Index')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Profile Details</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">New Post</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                          @csrf
                            <div class="form-group">
                                <label for="inputDescription">Describe Your Experience</label>
                                <textarea id="inputDescription" name="body" placeholder="What experience would you share?"
                                    class="form-control" rows="4"
                                    style="margin-top: 0px; margin-bottom: 0px; height: 141px;"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Post Picture</label>
                                <div class="input-group">
                                  <div class="custom-file">
                        
                                    <input type="file" name="file" id="" class=""><br>
                                    
                                  </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Show Picture</button>

                        </form>

                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <div class="image">
                                @if ($profile_details_process->photo_profile == '')
                                    <img src="https://ui-avatars.com/api/?name={{ $profile_details_process->full_name }}"
                                        class="img-circle elevation-2">
                                @else
                                    <img src="{{ $profile_details_process->photo_profile }}"
                                        class="img-circle elevation-2">
                                @endif
                            </div>
                        </div>

                        <h3 class="profile-username text-center">{{ $profile_details_process->full_name }}</h3>

                        <p class="text-muted text-center">{{ $profile_details_process->bio }}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b><a href="{{ route('follow.list', $profile_details_process->id) }}"> Followers </a></b>
                                <a class="float-right">{{ $total_followed_process }}</a>
                            </li>
                            <li class="list-group-item">
                                <b><a href="{{ route('following.list', $profile_details_process->id) }}"> Following
                                    </a></b> <a class="float-right">{{ $total_follow_process }}</a>
                            </li>
                        </ul>

                        <a href="#" class="btn btn-primary btn-block"><b>Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->

                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header p-2">
                        List Post
                    </div>
                    <div class="card-body">
                        @foreach ($post_list as $item)
                            <div class="post">
                                <div class="user-block">
                                    <div class="image">
                                        @if ($profile_details_process->photo_profile == '')
                                            <img src="https://ui-avatars.com/api/?name={{ $profile_details_process->full_name }}"
                                                class="img-circle elevation-2">
                                        @else
                                            <img src="{{ $profile_details_process->photo_profile }}"
                                                class="img-circle elevation-2">
                                        @endif
                                    </div>
                                    <span class="username">
                                        <a href="#">{{ $profile_details_process->full_name }}</a>
                                        <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                    </span>
                                    <span
                                        class="description">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    {{ $item->body }}
                                </p>

                                @if (!empty($item->picture))
                                <img class="img-fluid pad" src="{{ Storage::url($item->picture) }}" alt="Photo">
                                @else
                                    
                                @endif

                                <p>
                                    <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                    <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                    <span class="float-right">
                                        <a href="/comment" class="link-black text-sm">
                                            <i class="far fa-comments mr-1"></i> Comments
                                        </a>
                                    </span>
                                </p>
                            </div>

                        @endforeach

                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
@endsection

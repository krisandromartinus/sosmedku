@extends('template.master')
@section('title', 'Index')

@section('content')
    <div class="content-wrapper" style="min-height: 2010.5px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <h2 class="text-center display-4">Search</h2>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <form action="{{ route('search.user') }}">
                            <div class="input-group input-group-lg">
                                <input type="search" class="form-control form-control-lg"
                                    placeholder="Silahkan cari nama teman anda di sini" name="search" value="">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-10 offset-md-1">
                        <div class="list-group">
                            
                            @foreach ($user_search as $item_user)
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="image">
                                        @if ($item_user->profile->photo_profile == '')
                                            <img src="https://ui-avatars.com/api/?name={{ $item_user->name }}" class="img-circle elevation-2">
                                        @else
                                            <img src="https://ui-avatars.com/api/?name={{ $item_user->profile->photo_profile }}" class="img-circle elevation-2">
                                        @endif
                                        <img src="" class="img-circle elevation-2">
                                      </div>
                                    <div class="col px-4">
                                        <div>
                                            <div class="float-right">Bio : {{ $item_user->profile->bio }}</div>
                                            <h4><a href="{{ route('profile.data', $item_user->name) }}" >{{ $item_user->name }}</a></h4>
                                            <p>Followers : {{ $item_user->followed->count() }}</p>
                                            <p>Followed : {{ $item_user->follow->count() }}</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

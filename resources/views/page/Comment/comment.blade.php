@extends('template.master')
@section('title', 'Index')

@section('content')
<div>
    <h2>Tambah Data</h2>
        <form action="/comment" method="POST">
            @csrf

            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header p-2">
                        Picture
                    </div>
                    <div class="card-body">
                        @foreach ($post_list as $item)
                            <div class="post">
                                <div class="user-block">
                                    <div class="image">
                                        @if ($profile_details_process->photo_profile == '')
                                            <img src="https://ui-avatars.com/api/?name={{ $profile_details_process->full_name }}"
                                                class="img-circle elevation-2">
                                        @else
                                            <img src="{{ $profile_details_process->photo_profile }}"
                                                class="img-circle elevation-2">
                                        @endif
                                    </div>
                                    <span class="username">
                                        <a href="#">{{ $profile_details_process->full_name }}</a>
                                        <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                    </span>
                                    <span
                                        class="description">{{ Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</span>
                                </div>
                                <!-- /.user-block -->
                                <p>
                                    {{ $item->body }}
                                </p>

                                @if (!empty($item->picture))
                                <img class="img-fluid pad" src="{{ Storage::url($item->picture) }}" alt="Photo">
                                @else
                                    
                                @endif

                                <p>
                                    <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                    <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Like</a>
                                    <span class="float-right">
                                        <a href="#" class="link-black text-sm">
                                            <i class="far fa-comments mr-1"></i> Comments
                                        </a>
                                    </span>
                                </p>
                                <input class="form-control form-control-sm" type="text" placeholder="Type a comment">
                                <a href="#" class="btn btn-link">Comment</a>
                            </div>
                        @endforeach
        </form>
</div>
@endsection

@include('template.partials.header')
@include('template.partials.navbar')
@include('template.partials.sidebar')
@include('sweetalert::alert')
<div class="content-wrapper">
    @yield('content')
</div>

@include('template.partials.footer')

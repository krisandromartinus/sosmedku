<?php

use App\Http\Controllers\FollowController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TimelineController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('login');
});

/**
 * Route untuk menangani user
 * yang belom terAutentikasi
 */
Route::middleware(['guest'])->group(function () {

    /**
     * Register
     */
    Route::get('register', [RegisterController::class, 'index'])->name('register.index');
    Route::post('register', [RegisterController::class, 'store'])->name('register.store');

    /**
     * Login
     */
    Route::get('/login', [LoginController::class, 'index'])->name('login');
    Route::post('/login', [LoginController::class, 'auth'])->name('login.auth');

});

Route::middleware(['auth'])->group(function () {

    /**
     * Profile
     */

    Route::resource('profile', ProfileController::class);


    /**
     * Posts
     */
    Route::resource('/post', PostController::class);
    Route::get('/post/{id}', 'PostController@show')->name('post.show');

    /**
     * Timeline
     */
    Route::get('timeline', [TimelineController::class, 'index'])->name('timeline.index');

    /**
     * Follow
     */
    Route::post('follow/{id}/user', [FollowController::class, 'followUser'])->name('follow.user');
    Route::delete('follow/{id}/user', [FollowController::class, 'unfollowUser'])->name('unfollow.user');

    /**
     * Likes
     */
    Route::get('like/{id}', [LikeController::class, 'likePost'])->name('like.post');

    /**
     * List Followers
     */
    Route::get('/follow/{id}/list', [FollowController::class,'followList'])->name('follow.list');
    Route::get('/following/{id}/list', [FollowController::class,'followedList'])->name('following.list');

    /**
     * Search
     */
    Route::get('search', [SearchController::class, 'searchUser'])->name('search.user');


    /**
     * Get User Profile
     */

    Route::get('/{profile}', [ProfileController::class, 'profileData'])->name('profile.data');

    

    /**
     * Comment
     */
    Route::post('/comment', 'CommentController@index');
});
